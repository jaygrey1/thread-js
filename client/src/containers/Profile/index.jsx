import React from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
    Grid, Image, Input, List, Modal,
    Button, Icon, Confirm, Popup
} from 'semantic-ui-react';

import { NotificationManager } from 'react-notifications';

import ReactCrop from 'react-image-crop';
import 'react-image-crop/lib/ReactCrop.scss';

import UserStatus from 'src/components/UserStatus';

import validator from 'validator';

import { updateUserInPosts } from 'src/containers/Thread/actions';
import { updateUsername, updateUserPicture, updateStatus } from './actions';


const initialState = {
    editUsernamePressed: false,
    editUsernameField: false,
    openConfirmChange: false,
    isUsernameValid: true,
    updateProfilePhotoModal: false,
    profileImageSource: null,
    uploadingInProgess: false
};

const validate = (string) => {
    const pattern = '^[a-zA-Z]\\w{1,20}$';
    return validator.matches(validator.trim(string), pattern, 'gm');
};

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ...initialState,
            newUsername: props.user.username,
            crop: {
                unit: 'px',
                width: 50,
                aspect: 1
            }
        };
        this.fileInput = '';
        this.image = null;
    }

    onCropChange = (crop) => {
        this.setState({ crop });
    };

    onImageLoad = image => this.image = image;

    handleEditUsername() {
        this.setState(
            (state, props) => {
                const newState = {
                    editUsernamePressed: !state.editUsernamePressed,
                    editUsernameField: !state.editUsernameField,
                    isUsernameValid: true,
                };
                if (!newState.editUsernamePressed) {
                    if (state.newUsername !== props.user.username && state.isUsernameValid) {
                        // send
                        return { ...newState, openConfirmChange: true };
                    }
                    return { ...newState, newUsername: props.user.username };
                }

                return newState;
            }
        );
    }

    handleChangeUserName(data) {
        this.setState({
            newUsername: data,
            isUsernameValid: validate(data)
        });
    }

    resetEditUsername() {
        this.setState((state, props) => ({
            ...initialState,
            newUsername: props.user.username
        }));
    }

    async confirmEditUsername() {
        const { isUsernameValid, newUsername } = this.state;
        if (isUsernameValid) {
            const result = await this.props.updateUsername(validator.trim(newUsername));
            if (!result.error) {
                this.setState({ ...initialState, newUsername });
                await this.props.updateUserInPosts(this.props.user);
            } else {
                NotificationManager.error('Update error', result.message);
            }
        }

        this.resetEditUsername();
    }

    handleUploadFile({ target }) {
        const file = target.files[0];

        const reader = new FileReader();
        reader.addEventListener('load', () => {
            this.setState({ profileImageSource: reader.result, updateProfilePhotoModal: true });
        });

        reader.readAsDataURL(file);
    }

    handleUpdateProfilePictrure() {
        const { crop } = this.state;
        const canvas = document.createElement('canvas');
        const scaleX = this.image.naturalWidth / this.image.width;
        const scaleY = this.image.naturalHeight / this.image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const context = canvas.getContext('2d');

        context.drawImage(this.image,
            crop.x * scaleX, crop.y * scaleY,
            crop.width * scaleX, crop.height * scaleY,
            0, 0,
            crop.width, crop.height);

        this.setState({ uploadingInProgess: true });
        try {
            canvas.toBlob(blob => this.props.updateUserPicture(
                blob,
                () => this.handleFinishUpload()
            ));
        } catch (error) {
            this.handleFinishUpload();
        }
    }

    handleFinishUpload() {
        this.setState({ updateProfilePhotoModal: false, uploadingInProgess: false });
    }

    render() {
        const { user } = this.props;
        const {
            editUsernamePressed,
            editUsernameField,
            newUsername,
            isUsernameValid,
            profileImageSource,
            crop,
            updateProfilePhotoModal
        } = this.state;

        return (
            <Grid container textAlign="center" style={{ paddingTop: 30 }}>

                <Grid.Row>
                    <Grid.Column>
                        <Image centered src={getUserImgLink(user.image)} size="medium" circular />
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row columns={2}>
                    <Grid.Column>
                        <Popup
                            trigger={(
                                <Input
                                    icon="user"
                                    iconPosition="left"
                                    placeholder="Username"
                                    type="text"
                                    disabled={!editUsernameField}
                                    value={newUsername}
                                    onChange={
                                        (event, data) => this.handleChangeUserName(data.value)
                                    }
                                />
                            )}
                            style={{ color: 'red', borderColor: 'red' }}
                            position="left center"
                            open={!isUsernameValid}
                        >
                            <Popup.Header>Unacceptable username</Popup.Header>
                            <Popup.Content>
                                <List bulleted>
                                    <List.Item>Must start with a character</List.Item>
                                    <List.Item>Contains less than 20 characters</List.Item>
                                    <List.Item>Contains no spaces</List.Item>
                                    <List.Item>Acceptable chars: A..Z a..z 0-9 _</List.Item>
                                </List>
                            </Popup.Content>
                        </Popup>
                        <Button icon toggle attached="right" active={editUsernamePressed} onClick={() => this.handleEditUsername()}>
                            <Icon name="edit outline" />
                        </Button>
                        <Confirm
                            size="mini"
                            content="Do you really want to change your username ?"
                            open={this.state.openConfirmChange}
                            onCancel={() => this.resetEditUsername()}
                            onConfirm={() => this.confirmEditUsername()}
                        />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={4}>
                    <Grid.Column stretched>
                        <Input
                            icon="at"
                            iconPosition="left"
                            placeholder="Email"
                            type="email"
                            disabled
                            value={user.email}
                        />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={4}>
                    <Grid.Column stretched>
                        <UserStatus
                            text={user.status}
                        />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={4}>
                    <Grid.Column stretched>
                        <Modal
                            size="small"
                            open={updateProfilePhotoModal}
                            onClose={() => {
                                this.setState({
                                    updateProfilePhotoModal: false,
                                    profileImageSource: null
                                });
                                this.fileInput.value = '';
                            }}
                        >
                            <Modal.Header>Update profile image</Modal.Header>
                            <Modal.Content>
                                <Modal.Description>
                                    <Grid centered>
                                        <Grid.Column>
                                            <ReactCrop
                                                src={profileImageSource}
                                                crop={crop}
                                                maxWidth={200}
                                                maxHeight={200}
                                                onChange={data => this.setState({ crop: data })}
                                                onImageLoaded={this.onImageLoad}
                                            />
                                        </Grid.Column>
                                    </Grid>
                                </Modal.Description>
                            </Modal.Content>
                            <Modal.Actions>
                                <Button
                                    primary
                                    loading={this.state.uploadingInProgess}
                                    onClick={() => this.handleUpdateProfilePictrure()}
                                >
                                    Update
                                </Button>
                            </Modal.Actions>
                        </Modal>
                        <Button icon as="label">
                            <Icon name="image" />
                            Update profile picture
                            <input
                                name="image"
                                type="file"
                                hidden
                                ref={(ref) => { this.fileInput = ref; }}
                                onChange={data => this.handleUploadFile(data)}
                            />
                        </Button>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

Profile.propTypes = {
    user: PropTypes.objectOf(PropTypes.any),
    updateUsername: PropTypes.func,
    updateUserInPosts: PropTypes.func,
    updateUserPicture: PropTypes.func,
};

Profile.defaultProps = {
    user: {},
    updateUsername: () => { },
    updateUserInPosts: () => { },
    updateUserPicture: () => { },
};

const mapStateToProps = rootState => ({
    user: rootState.profile.user
});

const actions = {
    updateUsername,
    updateUserInPosts,
    updateUserPicture,
    updateStatus
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);


export default connect(
    mapStateToProps, mapDispatchToProps
)(Profile);
