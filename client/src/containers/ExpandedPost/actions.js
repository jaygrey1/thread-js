import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';

import {
    SET_UPDATED_COMMENT,
    UPDATE_COMMENT,
    DELETE_COMMENT,
    TOGGLE_COMMENT_LIKE,
    TOGGLE_COMMENT_DISLIKE,
    SET_WHO_LIKED_COMMENT
} from './actionTypes';

const setUpdatedCommentAction = comment => ({
    type: SET_UPDATED_COMMENT,
    comment,
});

const updateCommentAction = comment => ({
    type: UPDATE_COMMENT,
    comment
});

const deleteCommentAction = post => ({
    type: DELETE_COMMENT,
    post
});

const likeCommentAction = comment => ({
    type: TOGGLE_COMMENT_LIKE,
    comment
});

const dislikeCommentAction = comment => (
    {
        type: TOGGLE_COMMENT_DISLIKE,
        comment
    }
);

const setWhoLikedCommentAction = users => ({
    type: SET_WHO_LIKED_COMMENT,
    users
});

export const toggleUpdatedComment = commentId => async (dispatch) => {
    const comment = commentId ? await commentService.getComment(commentId) : undefined;
    dispatch(setUpdatedCommentAction(comment));
};

export const updateComment = comment => async (dispatch) => {
    const result = await commentService.updateComment(comment);
    if (result) {
        const newComment = await commentService.getComment(comment.id);
        dispatch(updateCommentAction(newComment));
    }
};

export const deleteComment = (commentId, postId) => async (dispatch) => {
    if (await commentService.deleteComment(commentId)) {
        const post = await postService.getPost(postId);
        dispatch(deleteCommentAction(post));
    }
};

export const likeComment = comment => async (dispath) => {
    const diffComment = await commentService.likeOrDislikeComment(comment.id, true);
    dispath(likeCommentAction(diffComment));
};

export const dislikeComment = comment => async (dispath) => {
    const diffComment = await commentService.likeOrDislikeComment(comment.id, false);
    dispath(dislikeCommentAction(diffComment));
};

export const toggleWhoLikedComment = commentId => async (dispatch) => {
    const users = commentId ? await commentService.getUsersWhoLikedComment(commentId) : undefined;
    dispatch(setWhoLikedCommentAction(users));
};
