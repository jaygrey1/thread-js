/* eslint-disable linebreak-style */
import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';
import UpdateComment from 'src/components/UpdateComment';
import LikedUsers from 'src/components/LikedUsers';

import {
    likeOrDislikePost,
    toggleExpandedPost,
    toggleUpdatedPost,
    addComment,
    toggleWhoLikedPost
} from 'src/containers/Thread/actions';

import {
    toggleUpdatedComment,
    updateComment,
    deleteComment,
    likeComment,
    dislikeComment,
    toggleWhoLikedComment
} from './actions';


class ExpandedPost extends React.Component {
    state = {
        open: true
    };

    closeModal = () => {
        this.props.toggleExpandedPost();
    }

    render() {
        const { post, sharePost, updatedComment, likedCommentUsers, ...props } = this.props;

        let updatedCommentModal;
        if (updatedComment) {
            updatedCommentModal = (
                <UpdateComment
                    comment={updatedComment}
                    updateHandler={props.updateComment}
                    toggleModalHandler={props.toggleUpdatedComment}
                />
            );
        }

        return (
            <Modal dimmer="blurring" centered={false} open={this.state.open} onClose={this.closeModal}>
                {post
                    ? (
                        <Modal.Content>
                            <Post
                                post={post}
                                likeOrDislikePost={props.likeOrDislikePost}
                                toggleExpandedPost={props.toggleExpandedPost}
                                toggleUpdatedPost={props.toggleUpdatedPost}
                                sharePost={sharePost}
                                showWhoLikedPost={props.toggleWhoLikedPost}
                            />
                            <CommentUI.Group style={{ maxWidth: '100%' }}>
                                <Header as="h3" dividing>Comments</Header>
                                {post.comments && post.comments
                                    .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                                    .map(comment => (
                                        <Comment
                                            key={comment.id}
                                            comment={comment}
                                            isOwner={comment.user.id === props.userId}
                                            toggleUpdateComment={props.toggleUpdatedComment}
                                            deleteComment={props.deleteComment}
                                            likeComment={props.likeComment}
                                            dislikeComment={props.dislikeComment}
                                            toggleWhoLikedComment={props.toggleWhoLikedComment}
                                        />
                                    ))
                                }
                                <AddComment postId={post.id} addComment={props.addComment} />
                            </CommentUI.Group>
                        </Modal.Content>
                    )
                    : <Spinner />
                }
                {
                    updatedCommentModal
                }
                {
                    likedCommentUsers && likedCommentUsers.length > 0
                    && (
                        <LikedUsers
                            header="Users who liked this comment"
                            users={likedCommentUsers}
                            close={props.toggleWhoLikedComment}
                        />
                    )
                }
            </Modal>
        );
    }
}

ExpandedPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    likeOrDislikePost: PropTypes.func.isRequired,
    addComment: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    updatedComment: PropTypes.objectOf(PropTypes.any),
    likeComment: PropTypes.func.isRequired,
    dislikeComment: PropTypes.func.isRequired,
    likedCommentUsers: PropTypes.arrayOf(PropTypes.any)
};

ExpandedPost.defaultProps = {
    updatedComment: undefined,
    likedCommentUsers: undefined
};

const mapStateToProps = rootState => ({
    post: rootState.posts.expandedPost,
    userId: rootState.profile.user.id,
    updatedComment: rootState.posts.updatedComment,
    likedCommentUsers: rootState.posts.likedCommentUsers
});

const actions = {
    likeOrDislikePost,
    toggleExpandedPost,
    toggleUpdatedPost,
    addComment,
    toggleUpdatedComment,
    deleteComment,
    updateComment,
    likeComment,
    dislikeComment,
    toggleWhoLikedPost,
    toggleWhoLikedComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ExpandedPost);
