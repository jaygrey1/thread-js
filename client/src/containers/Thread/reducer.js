import {
    SET_UPDATED_COMMENT,
    UPDATE_COMMENT,
    DELETE_COMMENT,
    TOGGLE_COMMENT_LIKE,
    TOGGLE_COMMENT_DISLIKE,
    SET_WHO_LIKED_COMMENT
} from 'src/containers/ExpandedPost/actionTypes';

import {
    SET_ALL_POSTS,
    LOAD_MORE_POSTS,
    ADD_POST,
    SET_EXPANDED_POST,
    UPDATE_POST,
    SET_UPDATED_POST,
    DELETE_POST,
    SET_WHO_LIKED_POST,
    UPDATE_USERNAME_IN_POSTS
} from './actionTypes';


export default (state = {}, action) => {
    switch (action.type) {
        case SET_ALL_POSTS:
            return {
                ...state,
                posts: action.posts,
                hasMorePosts: Boolean(action.posts.length)
            };
        case LOAD_MORE_POSTS:
            return {
                ...state,
                // concat two arrays without duplications
                posts: [...(state.posts || []), ...(state.posts === undefined
                    ? action.posts
                    : action.posts.filter(
                        elem => state.posts.findIndex(elem1 => elem.id === elem1.id) === -1
                    ))
                ],
                hasMorePosts: Boolean(action.posts.length)
            };
        case ADD_POST:
            return {
                ...state,
                posts: [action.post, ...state.posts]
            };
        case UPDATE_POST:
            return {
                ...state,
                posts: state.posts.map(post => (post.id === action.post.id ? action.post : post)),
            };
        case SET_EXPANDED_POST:
            return {
                ...state,
                expandedPost: action.post
            };
        case SET_UPDATED_POST: {
            return {
                ...state,
                updatedPost: action.post
            };
        }
        case DELETE_POST: {
            return {
                ...state,
                posts: state.posts.filter(post => post.id !== action.postId)
            };
        }
        case SET_UPDATED_COMMENT: {
            return {
                ...state,
                updatedComment: action.comment,
            };
        }
        case UPDATE_COMMENT: {
            return {
                ...state,
                expandedPost: {
                    ...state.expandedPost,
                    comments: state.expandedPost.comments.map(
                        comment => (comment.id === action.comment.id ? action.comment : comment)
                    ),
                }
            };
        }
        case DELETE_COMMENT: {
            return {
                ...state,
                posts: state.posts.map(post => (post.id === action.post.id ? action.post : post)),
                expandedPost: action.post
            };
        }
        case TOGGLE_COMMENT_LIKE:
        case TOGGLE_COMMENT_DISLIKE: {
            return {
                ...state,
                expandedPost: {
                    ...state.expandedPost,
                    comments: state.expandedPost.comments.map(
                        comment => (action.comment.commentId === comment.id ? {
                            ...comment,
                            likeCount: Number(comment.likeCount) + action.comment.like,
                            dislikeCount: Number(comment.dislikeCount) + action.comment.dislike
                        } : comment)
                    ),

                }
            };
        }
        case SET_WHO_LIKED_POST: {
            return {
                ...state,
                likedUsers: action.users
            };
        }
        case SET_WHO_LIKED_COMMENT: {
            return {
                ...state,
                likedCommentUsers: action.users
            };
        }
        case UPDATE_USERNAME_IN_POSTS: {
            return {
                ...state,
                posts: (state.posts || []).map(post => (post.user.id === action.user.id
                    ? { ...post, user: { ...post.user, username: action.user.username } }
                    : post)),
            };
        }
        default: {
            return state;
        }
    }
};
