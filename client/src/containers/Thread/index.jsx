import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import UpdatePost from 'src/components/UpdatePost';
import LikedUsers from 'src/components/LikedUsers';
import {
    loadPosts, loadMorePosts, likeOrDislikePost, toggleExpandedPost,
    toggleUpdatedPost, addPost, updatePost, deletePost, toggleWhoLikedPost
} from './actions';

import styles from './styles.module.scss';


class Thread extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedPostId: undefined,
            showOwnPosts: false,
            hideOwnPosts: false,
            likedByMePosts: false
        };
        this.postsFilter = {
            userId: undefined,
            excludeUserId: undefined,
            from: 0,
            count: 10
        };
    }

    toggleShowOwnPosts = () => {
        this.setState(
            ({ showOwnPosts }) => ({ showOwnPosts: !showOwnPosts }),
            this.updatePostsFilter
        );
    };

    toggleHideOwnPosts = () => {
        this.setState(
            state => ({ hideOwnPosts: !state.hideOwnPosts }),
            this.updatePostsFilter
        );
    }

    togglelikedByMePosts = () => {
        this.setState(
            state => ({ likedByMePosts: !state.likedByMePosts }),
            this.updatePostsFilter
        );
    }

    updatePostsFilter = () => {
        this.postsFilter = {
            ...this.postsFilter,
            userId: this.state.showOwnPosts ? true : undefined,
            excludeUserId: this.state.hideOwnPosts ? true : undefined,
            likedByUserId: this.state.likedByMePosts ? true : undefined,
            from: 0
        };
        this.props.loadPosts(this.postsFilter);
    }

    loadMorePosts = () => {
        this.props.loadMorePosts(this.postsFilter);
        const { from, count } = this.postsFilter;
        this.postsFilter.from = from + count;
    }

    sharePost = (sharedPostId) => {
        this.setState({ sharedPostId });
    };

    closeSharePost = () => {
        this.setState({ sharedPostId: undefined });
    }

    uploadImage = file => imageService.uploadImage(file);


    render() {
        const {
            posts = [],
            expandedPost,
            updatedPost,
            hasMorePosts,
            likedPostUsers,
            ...props
        } = this.props;

        const { showOwnPosts, sharedPostId, hideOwnPosts, likedByMePosts } = this.state;

        return (
            <div className={styles.threadContent}>
                <div className={styles.addPostForm}>
                    <AddPost addPost={props.addPost} uploadImage={this.uploadImage} />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Show my posts" checked={showOwnPosts} onChange={this.toggleShowOwnPosts} />
                    <Checkbox toggle label="Hide my posts" checked={hideOwnPosts} onChange={this.toggleHideOwnPosts} className={styles.checkbox} />
                    <Checkbox toggle label="Liked by me" checked={likedByMePosts} onChange={this.togglelikedByMePosts} className={styles.checkbox} />
                </div>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMorePosts}
                    hasMore={hasMorePosts}
                    loader={<Loader active inline="centered" key={0} />}
                >
                    {posts.map(post => (
                        <Post
                            post={post}
                            likeOrDislikePost={props.likeOrDislikePost}
                            toggleExpandedPost={props.toggleExpandedPost}
                            toggleUpdatedPost={props.toggleUpdatedPost}
                            sharePost={this.sharePost}
                            deletePost={props.deletePost}
                            key={post.id}
                            isOwner={post.userId === this.props.userId}
                            showWhoLikedPost={props.toggleWhoLikedPost}
                        />
                    ))}
                </InfiniteScroll>
                {
                    expandedPost
                    && <ExpandedPost sharePost={this.sharePost} />
                }
                {
                    sharedPostId
                    && <SharedPostLink postId={sharedPostId} close={this.closeSharePost} />
                }
                {
                    updatedPost
                    && (
                        <UpdatePost
                            post={updatedPost}
                            updateHandler={props.updatePost}
                            toggleUpdatedPost={props.toggleUpdatedPost}
                        />
                    )
                }
                {
                    likedPostUsers && likedPostUsers.length > 0
                    && (
                        <LikedUsers
                            header="Users who liked this post"
                            users={likedPostUsers}
                            close={props.toggleWhoLikedPost}
                        />
                    )
                }
            </div>
        );
    }
}

Thread.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    hasMorePosts: PropTypes.bool,
    expandedPost: PropTypes.objectOf(PropTypes.any),
    updatedPost: PropTypes.objectOf(PropTypes.any),
    updatePost: PropTypes.func.isRequired,
    sharedPostId: PropTypes.string,
    userId: PropTypes.string,
    loadPosts: PropTypes.func.isRequired,
    loadMorePosts: PropTypes.func.isRequired,
    likeOrDislikePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    addPost: PropTypes.func.isRequired,
    likedPostUsers: PropTypes.arrayOf(PropTypes.any),
};

Thread.defaultProps = {
    posts: [],
    hasMorePosts: true,
    expandedPost: undefined,
    updatedPost: undefined,
    sharedPostId: undefined,
    userId: undefined,
    likedPostUsers: []
};

const mapStateToProps = rootState => ({
    posts: rootState.posts.posts,
    hasMorePosts: rootState.posts.hasMorePosts,
    expandedPost: rootState.posts.expandedPost,
    updatedPost: rootState.posts.updatedPost,
    updatedComment: rootState.posts.updatedComment,
    userId: rootState.profile.user.id,
    likedPostUsers: rootState.posts.likedUsers,
});

const actions = {
    loadPosts,
    loadMorePosts,
    likeOrDislikePost,
    toggleExpandedPost,
    toggleUpdatedPost,
    addPost,
    updatePost,
    deletePost,
    toggleWhoLikedPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Thread);
