import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
    ADD_POST,
    LOAD_MORE_POSTS,
    SET_ALL_POSTS,
    SET_EXPANDED_POST,
    UPDATE_POST,
    SET_UPDATED_POST,
    DELETE_POST,
    SET_WHO_LIKED_POST,
    UPDATE_USERNAME_IN_POSTS
} from './actionTypes';

const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const updatePostAction = post => ({
    type: UPDATE_POST,
    post
});

const setUpdatedPostAction = post => ({
    type: SET_UPDATED_POST,
    post
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

const deletePostAction = postId => ({
    type: DELETE_POST,
    postId
});

const setWhoLikedPostAction = users => ({
    type: SET_WHO_LIKED_POST,
    users
});

const updateUserAction = user => ({
    type: UPDATE_USERNAME_IN_POSTS,
    user
});

export const loadPosts = filter => async (dispatch) => {
    const posts = await postService.getAllPosts(filter);
    dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch) => {
    const posts = await postService.getAllPosts(filter);
    dispatch(addMorePostsAction(posts));
};

export const applyPost = postId => async (dispatch) => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async (dispatch) => {
    const { id } = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    dispatch(addPostAction(newPost));
};

export const updatePost = post => async (dispatch) => {
    await postService.updatePost(post);
    const newPost = await postService.getPost(post.id);
    dispatch(updatePostAction(newPost));
};

export const toggleExpandedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setExpandedPostAction(post));
};

export const toggleUpdatedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setUpdatedPostAction(post));
};

export const likeOrDislikePost = (postId, reaction) => async (dispatch, getRootState) => {
    await postService.likeOrDislikePost(postId, reaction);
    const updatedPost = await postService.getPost(postId);

    const mapLikes = post => ({
        ...post,
        likeCount: updatedPost.likeCount, // diff is taken from the current closure
        dislikeCount: updatedPost.dislikeCount
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapLikes(expandedPost)));
    }
};

export const deletePost = postId => async (dispatch) => {
    if (postService.deletePost(postId)) {
        dispatch(deletePostAction(postId));
    }
};

export const addComment = request => async (dispatch, getRootState) => {
    const { id } = await commentService.addComment(request);
    const comment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== comment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const toggleWhoLikedPost = postId => async (dispatch) => {
    const users = postId ? await postService.getUsersWhoLikedPost(postId) : undefined;
    dispatch(setWhoLikedPostAction(users));
};

export const updateUserInPosts = user => async (dispatch) => {
    dispatch(updateUserAction(user));
};
