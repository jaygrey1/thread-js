import callWebApi from 'src/helpers/webApiHelper';
import { uploadImage } from './imageService';


const updateUser = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/user',
        type: 'PUT',
        request
    });
    return response.json();
};

export const updateUsername = async username => updateUser({ username });


export const updateUserPicture = async (image) => {
    const newImage = await uploadImage(image);
    return updateUser({ imageId: newImage.id });
};

export const updateUserStatus = async status => updateUser({ status });
