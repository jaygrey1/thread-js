import callWebApi from 'src/helpers/webApiHelper';

export const getAllPosts = async (filter) => {
    const response = await callWebApi({
        endpoint: '/api/posts',
        type: 'GET',
        query: filter
    });
    return response.json();
};

export const addPost = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/posts',
        type: 'POST',
        request
    });
    return response.json();
};


export const getPost = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/posts/${id}`,
        type: 'GET'
    });
    return response.json();
};

export const updatePost = async (post) => {
    const response = await callWebApi({
        endpoint: `/api/posts/${post.id}`,
        type: 'PUT',
        request: post
    });
    return response.json();
};

export const likeOrDislikePost = async (postId, reaction) => {
    const response = await callWebApi({
        endpoint: '/api/posts/react',
        type: 'PUT',
        request: {
            postId,
            isLike: reaction
        }
    });
    return response.json();
};

export const deletePost = async (postId) => {
    const response = await callWebApi({
        endpoint: `/api/posts/${postId}`,
        type: 'DELETE',
    });

    return response.ok;
};

export const getUsersWhoLikedPost = async (postId) => {
    const response = await callWebApi({
        endpoint: `/api/posts/who-likes/${postId}`,
        type: 'GET',
    });

    return response.json();
};

// should be replaced by approppriate function
export const getPostByHash = async hash => getPost(hash);

export const sharePostLinkViaEmail = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/posts/share/email',
        type: 'POST',
        request
    });
    return response.json();
};
