import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/comments',
        type: 'POST',
        request
    });
    return response.json();
};

export const getComment = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: 'GET'
    });
    return response.json();
};

export const updateComment = async (comment) => {
    try {
        const response = await callWebApi({
            endpoint: `/api/comments/${comment.id}`,
            type: 'PUT',
            request: comment
        });
        return response.ok;
    } catch (e) {
        return false;
    }
};

export const deleteComment = async (commentId) => {
    try {
        const response = await callWebApi({
            endpoint: `/api/comments/${commentId}`,
            type: 'DELETE'
        });
        return response.ok;
    } catch (e) {
        return false;
    }
};

export const likeOrDislikeComment = async (commentId, reaction) => {
    const response = await callWebApi({
        endpoint: '/api/comments/react',
        type: 'PUT',
        request: {
            commentId,
            isLike: reaction
        }
    });
    return response.json();
};

export const getUsersWhoLikedComment = async (commentId) => {
    const response = await callWebApi({
        endpoint: `/api/comments/who-likes/${commentId}`,
        type: 'GET',
    });

    return response.json();
};
