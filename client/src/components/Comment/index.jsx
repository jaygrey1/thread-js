import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = (props) => {
    const {
        comment: { id: commentId, body, createdAt, user, postId, likeCount, dislikeCount },
        isOwner,
        toggleUpdateComment,
        deleteComment,
        likeComment,
        dislikeComment,
        toggleWhoLikedComment
    } = props;

    const date = moment(createdAt).fromNow();

    let likersButton;
    if (likeCount > 0) {
        likersButton = (
            <CommentUI.Action size="small" onClick={() => toggleWhoLikedComment(commentId)}>
                <Icon name="eye" />
            </CommentUI.Action>
        );
    }

    return (
        <CommentUI className={styles.comment}>
            <CommentUI.Avatar src={getUserImgLink(user.image)} />
            <CommentUI.Content>
                <CommentUI.Author as="a">
                    {user.username}
                </CommentUI.Author>
                <CommentUI.Metadata>
                    {date}
                    {' - '}
                    {user.status}
                </CommentUI.Metadata>
                <CommentUI.Text>
                    {body}
                </CommentUI.Text>

                <CommentUI.Actions>
                    <CommentUI.Action size="small" onClick={() => likeComment(props.comment)}>
                        <Icon name="thumbs up" />
                        {likeCount}
                    </CommentUI.Action>
                    <CommentUI.Action size="small" onClick={() => dislikeComment(props.comment)}>
                        <Icon name="thumbs down" />
                        {dislikeCount}
                    </CommentUI.Action>
                    {isOwner && (
                        <React.Fragment>
                            <CommentUI.Action size="small" onClick={() => toggleUpdateComment(commentId)}>
                                <Icon name="pencil" />
                                Edit
                            </CommentUI.Action>
                            <CommentUI.Action size="small" onClick={() => deleteComment(commentId, postId)}>
                                <Icon name="trash" />
                                Delete
                            </CommentUI.Action>
                        </React.Fragment>
                    )}
                    {likersButton}
                </CommentUI.Actions>

            </CommentUI.Content>
        </CommentUI>
    );
};

Comment.defaultProps = {
    isOwner: false,
    deleteComment: () => { },
};

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired,
    isOwner: PropTypes.bool,
    toggleUpdateComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func,
    likeComment: PropTypes.func.isRequired,
    dislikeComment: PropTypes.func.isRequired,
    toggleWhoLikedComment: PropTypes.func.isRequired
};

export default Comment;
