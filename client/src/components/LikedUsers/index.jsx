import React from 'react';
import PropTypes from 'prop-types';
import { Modal, List, Image } from 'semantic-ui-react';
import { getUserOrDefaultLink } from 'src/helpers/imageHelper';


const LikedUsers = ({ header, users, close }) => (
    <Modal open size="mini" onClose={() => close()}>
        <Modal.Header>
            <span>{header}</span>
        </Modal.Header>
        <Modal.Content>
            <List>
                {
                    users.map(user => (
                        <List.Item key={user.username}>
                            <Image avatar src={getUserOrDefaultLink(user.link)} />
                            <List.Content>
                                <List.Header as="a">{user.username}</List.Header>
                            </List.Content>
                        </List.Item>
                    ))
                }
            </List>
        </Modal.Content>
    </Modal>
);

LikedUsers.defaultProps = {
    header: 'Liked users',
    users: []
};

LikedUsers.propTypes = {
    header: PropTypes.string,
    users: PropTypes.arrayOf(PropTypes.any),
    close: PropTypes.func.isRequired
};

export default LikedUsers;
