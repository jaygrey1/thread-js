import React from 'react';
import { Redirect, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import validator from 'validator';
import Logo from 'src/components/Logo';
import { NotificationManager } from 'react-notifications';

import * as authService from 'src/services/authService';

import {
    Grid,
    Header,
    Form,
    Button,
    Segment,
    Message
} from 'semantic-ui-react';

class ResetPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            isLoading: false,
            isEmailValid: true,
            isSent: false
        };
    }

    validateEmail = () => {
        const { email } = this.state;
        const isEmailValid = !validator.isEmpty(email);
        this.setState({ isEmailValid });
        return isEmailValid;
    };

    emailChanged = email => this.setState({ email, isEmailValid: true });

    validateForm = () => [
        this.validateEmail(),
    ].every(Boolean);

    handleClickResetPassword = async () => {
        const { isLoading, email } = this.state;
        const valid = this.validateForm();
        if (!valid || isLoading) {
            return;
        }
        this.setState({ isLoading: true });
        try {
            const result = await authService.resetPassword({ email });
            this.setState({ isSent: true });
            if (result.error) {
                NotificationManager.error('Error', result.message);
            }
        } catch (error) {
            // TODO: show error
            console.error(error);
            NotificationManager.error('Send error', error.toString());
        } finally {
            this.setState({ isLoading: false });
        }
    }

    render() {
        const { isLoading, isEmailValid, isSent } = this.state;
        return !this.props.isAuthorized
            ? (
                <Grid textAlign="center" verticalAlign="middle" className="fill">
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Logo />
                        <Header as="h2" color="teal" textAlign="center">
                            Reset password
                        </Header>
                        <Form name="loginForm" size="large" onSubmit={this.handleClickResetPassword}>
                            <Segment>
                                <Form.Input
                                    fluid
                                    icon="at"
                                    iconPosition="left"
                                    placeholder="Email"
                                    type="email"
                                    error={!isEmailValid}
                                    onChange={ev => this.emailChanged(ev.target.value)}
                                    onBlur={this.validateEmail}
                                />
                                <Button type="submit" disabled={isSent} color="teal" fluid size="large" loading={isLoading} primary>
                                    {isSent ? 'Email sent' : 'Send email'}
                                </Button>
                            </Segment>
                        </Form>
                        <Message>
                            Alredy with us?
                            {' '}
                            <NavLink exact to="/login">Sign In</NavLink>
                        </Message>
                        <Message>
                            New to us?
                            {' '}
                            <NavLink exact to="/registration">Sign Up</NavLink>
                        </Message>
                    </Grid.Column>
                </Grid>
            )
            : <Redirect to="/" />;
    }
}

ResetPassword.propTypes = {
    isAuthorized: PropTypes.bool
};

ResetPassword.defaultProps = {
    isAuthorized: false
};

export default ResetPassword;
