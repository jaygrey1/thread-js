/* eslint-disable linebreak-style */
import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Confirm } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

export default class Post extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            deleteConfirm: false
        };
    }

    render() {
        const { post, likeOrDislikePost, toggleExpandedPost, toggleUpdatedPost,
            sharePost, deletePost, isOwner, showWhoLikedPost } = this.props;

        const {
            id,
            image,
            body,
            user,
            likeCount,
            dislikeCount,
            commentCount,
            createdAt
        } = post;

        const date = moment(createdAt).fromNow();

        let edit;
        let deleteButton;

        if (isOwner) {
            edit = (
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleUpdatedPost(id)}>
                    <Icon name="pencil" />
                </Label>
            );

            deleteButton = (
                <>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => this.setState({ deleteConfirm: true })}>
                        <Icon name="trash" />
                    </Label>
                    <Confirm
                        size="mini"
                        content="Do you really want to delete your post  ?"
                        open={this.state.deleteConfirm}
                        onConfirm={() => deletePost(id)}
                        onCancel={() => this.setState({ deleteConfirm: false })}
                    />
                </>
            );
        }

        let likersButton;
        if (likeCount > 0) {
            likersButton = (
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => showWhoLikedPost(id)}>
                    <Icon name="eye" />
                </Label>
            );
        }

        return (
            <Card style={{ width: '100%' }}>
                {image && <Image src={image.link} wrapped ui={false} />}
                <Card.Content>
                    <Card.Meta>
                        <span className="date">
                            posted by
                                {' '}
                            {user.username}
                            {' - '}
                            {date}
                        </span>
                    </Card.Meta>
                    <Card.Description>
                        {body}
                    </Card.Description>
                </Card.Content>
                <Card.Content extra>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeOrDislikePost(id, true)}>
                        <Icon name="thumbs up" />
                        {likeCount}
                    </Label>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeOrDislikePost(id, false)}>
                        <Icon name="thumbs down" />
                        {dislikeCount}
                    </Label>

                    {likersButton}

                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
                        <Icon name="comment" />
                        {commentCount}
                    </Label>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
                        <Icon name="share alternate" />
                    </Label>
                    {edit}
                    {deleteButton}
                </Card.Content>
            </Card>
        );
    }
}

Post.defaultProps = {
    isOwner: false,
    deletePost: () => { }
};

Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    likeOrDislikePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    toggleUpdatedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    deletePost: PropTypes.func,
    isOwner: PropTypes.bool,
    showWhoLikedPost: PropTypes.func.isRequired,
};
