import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, TextArea, Form, Segment, Image, Icon } from 'semantic-ui-react';

import * as imageService from 'src/services/imageService';


class UpdatePost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            body: props.post.body,
        };
        this.fileInput = '';
    }

    closeModal = () => {
        this.props.toggleUpdatedPost();
    }

    handleChange(data) {
        this.setState({
            body: data,
        });
    }

    handleUpdateButton() {
        this.props.updateHandler({
            id: this.props.post.id,
            userId: this.props.post.userId,
            body: this.state.body
        });
        this.props.toggleUpdatedPost();
    }

    handleDeleteImage() {
        this.props.updateHandler({
            id: this.props.post.id,
            userId: this.props.post.userId,
            imageId: null
        });
        this.props.toggleUpdatedPost();
    }

    async handleUploadImage({ target }) {
        const file = target.files[0];

        try {
            const { id: imageId } = await imageService.uploadImage(file);

            await this.props.updateHandler({
                id: this.props.post.id,
                userId: this.props.post.userId,
                imageId
            });
        } finally {
            this.props.toggleUpdatedPost();
        }
    }

    render() {
        const { post } = this.props;

        return (
            <Modal open size="small">
                <Modal.Header>
                    <span>Update post</span>
                </Modal.Header>
                <Modal.Content>
                    <Form>
                        {
                            post.image
                            && (<Image src={post.image.link} />)
                        }
                        <TextArea
                            value={this.state.body}
                            onChange={(event, data) => this.handleChange(data.value)}
                        />
                    </Form>
                    <Segment size="small" color="blue">
                        {
                            post.image
                                ? (<Button size="tiny" onClick={() => this.handleDeleteImage()}>delete image</Button>)
                                : (
                                    <Button icon as="label" size="tiny">
                                        <Icon name="image" />
                                        add image
                                        <input
                                            name="image"
                                            type="file"
                                            hidden
                                            ref={(ref) => { this.fileInput = ref; }}
                                            onChange={data => this.handleUploadImage(data)}
                                        />
                                    </Button>
                                )
                        }
                    </Segment>
                </Modal.Content>
                <Modal.Actions>
                    <Button primary onClick={() => this.handleUpdateButton()}>
                        Update
                    </Button>
                    <Button secondary onClick={() => this.closeModal()}>Cancel</Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

UpdatePost.propTypes = {
    toggleUpdatedPost: PropTypes.func.isRequired,
    updateHandler: PropTypes.func.isRequired,
    post: PropTypes.shape({
        id: PropTypes.string,
        userId: PropTypes.string,
        body: PropTypes.string,
        image: PropTypes.object
    }).isRequired,
};

export default UpdatePost;
