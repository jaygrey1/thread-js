import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, TextArea, Form } from 'semantic-ui-react';


export default class UpdateComment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            body: props.comment.body,
        };
    }

    handleCloseButton = () => this.props.toggleModalHandler();

    handleUpdateButton = () => {
        this.props.updateHandler({ ...this.props.comment, body: this.state.body });
        this.props.toggleModalHandler();
    };

    handleUpdateText = (data) => {
        this.setState({
            body: data,
        });
    };

    render() {
        return (
            <Modal open size="small">
                <Modal.Header>
                    <span>Update comment</span>
                </Modal.Header>
                <Modal.Content>
                    <Form>
                        <TextArea
                            value={this.state.body}
                            onChange={(event, data) => this.handleUpdateText(data.value)}
                        />
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button primary onClick={() => this.handleUpdateButton()}>
                        Update
                    </Button>
                    <Button secondary onClick={() => this.handleCloseButton()}>
                        Cancel
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

UpdateComment.propTypes = {
    toggleModalHandler: PropTypes.func.isRequired,
    updateHandler: PropTypes.func.isRequired,
    comment: PropTypes.shape({
        body: PropTypes.string,
    }).isRequired,
};
