import React from 'react';
import { Redirect, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import validator from 'validator';
import * as queryString from 'query-string';
import Logo from 'src/components/Logo';
import * as authService from 'src/services/authService';

import {
    Grid,
    Header,
    Form,
    Button,
    Segment,
    Message
} from 'semantic-ui-react';

class ChangePassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: '',
            password1: '',
            password2: '',
            isTokenValid: true,
            isPasswordValid: true,
            isLoading: false,
            isSent: false,
            isUpdatedsuccessful: false
        };
        const parsed = queryString.parse(props.location.search);
        this.state.token = parsed.token || '';
    }

    validateToken = () => {
        this.setState(state => ({ isTokenValid: !validator.isEmpty(state.token) }));
        return this.state.isTokenValid;
    };

    validatePassword = () => {
        this.setState(state => (
            {
                isPasswordValid:
                    !(validator.isEmpty(state.password1)
                        || !validator.equals(state.password1, state.password2))
            }
        ));

        return this.state.isPasswordValid;
    }


    tokenChanged = token => this.setState({ token, isTokenValid: true });

    passwordChanged =
        (field, password) => this.setState({ [field]: password, isPasswordValid: true });


    validateForm = () => [
        this.validateToken(),
        this.validatePassword()
    ].every(Boolean);

    handleClickChangePassword = async () => {
        const { isLoading, token, password1: password } = this.state;
        const valid = this.validateForm();
        if (!valid || isLoading) {
            return;
        }
        this.setState({ isLoading: true });
        try {
            const result = await authService.changePassword({ token, password });
            if (!result.error) {
                this.setState({ isUpdatedsuccessful: true });
            }
        } catch (error) {
            console.log(error);
        } finally {
            this.setState({ isLoading: false });
        }
    }

    render() {
        const { isLoading, isTokenValid, isSent,
            isPasswordValid, token, isUpdatedsuccessful } = this.state;

        return !isUpdatedsuccessful
            ? (
                <Grid textAlign="center" verticalAlign="middle" className="fill">
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Logo />
                        <Header as="h2" color="teal" textAlign="center">Change password</Header>
                        <Form name="loginForm" size="large" onSubmit={this.handleClickChangePassword}>
                            <Segment>
                                <Form.Input
                                    fluid
                                    icon="lock"
                                    iconPosition="left"
                                    placeholder="token"
                                    type="text"
                                    value={token}
                                    error={!isTokenValid}
                                    onChange={ev => this.tokenChanged(ev.target.value)}
                                    onBlur={this.validateToken}
                                />
                                <Form.Input
                                    fluid
                                    icon="key"
                                    iconPosition="left"
                                    placeholder="new password"
                                    type="password"
                                    error={!isPasswordValid}
                                    onChange={ev => this.passwordChanged('password1', ev.target.value)}
                                    onBlur={this.validatePassword}
                                />
                                <Form.Input
                                    fluid
                                    icon="key"
                                    iconPosition="left"
                                    placeholder="retype new password"
                                    type="password"
                                    error={!isPasswordValid}
                                    onChange={ev => this.passwordChanged('password2', ev.target.value)}
                                    onBlur={this.validatePassword}
                                />

                                <Button type="submit" disabled={isSent} color="teal" fluid size="large" loading={isLoading} primary>
                                    {isSent ? 'Updated' : 'Update password'}
                                </Button>
                            </Segment>
                        </Form>
                        <Message>
                            Already with us?
                            {' '}
                            <NavLink exact to="/login">Sign In</NavLink>
                        </Message>
                        <Message>
                            New to us?
                            {' '}
                            <NavLink exact to="/registration">Sign Up</NavLink>
                        </Message>
                    </Grid.Column>
                </Grid>
            )
            : <Redirect to="/login" />;
    }
}

ChangePassword.propTypes = {
    location: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ChangePassword;
