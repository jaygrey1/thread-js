import * as authService from 'src/services/authService';
import * as userService from 'src/services/userService';

import { SET_USER } from '../../containers/Profile/actionTypes';


const setUserAction = user => ({
    type: SET_USER,
    user
});

export const updateStatus = newStatus => async (dispatch) => {
    await userService.updateUserStatus(newStatus);
    const user = await authService.getCurrentUser();
    dispatch(setUserAction(user));
};
