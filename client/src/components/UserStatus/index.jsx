import React from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Input, Label } from 'semantic-ui-react';

import { updateStatus } from './actions';

import styles from './styles.module.scss';


class UserStatus extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentValue: this.props.text,
            locked: true
        };
    }

    onFocusLost() {
        this.setState({ locked: true });
        if (this.props.text !== this.state.currentValue) {
            this.props.updateStatus(this.state.currentValue);
        }
    }

    onStatusChange({ value }) {
        this.setState({ currentValue: value });
    }

    render() {
        const labelElement = (
            <Label
                className={styles.userStatus}
                basic
                content={this.props.text}
                onClick={(e) => {
                    this.setState({ locked: false });
                    e.preventDefault();
                }}
            />
        );

        const inputElement = (
            <Input
                focus
                size="mini"
                className={styles.userStatus}
                value={this.state.currentValue}
                onClick={(e) => {
                    e.preventDefault();
                }}
                onBlur={() => this.onFocusLost()}
                onChange={(event, data) => this.onStatusChange(data)}
            />
        );


        return this.state.locked ? labelElement : inputElement;
    }
}

UserStatus.defaultProps = {
    text: ''
};

UserStatus.propTypes = {
    text: PropTypes.string,
    updateStatus: PropTypes.func.isRequired
};

const actions = {
    updateStatus
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(null, mapDispatchToProps)(UserStatus);
