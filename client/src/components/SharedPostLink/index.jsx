import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Input, Icon, Button, Divider } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import validator from 'validator';
import * as postService from 'src/services/postService';

import styles from './styles.module.scss';


class SharedPostLink extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            copied: false,
            email: '',
            isEmailValid: true,
            isEmailModalClosed: true
        };

        this.postLink = `${window.location.origin}/share/${props.postId}`;
    }

    copyToClipboard = (e) => {
        this.input.select();
        document.execCommand('copy');
        e.target.focus();
        this.setState({ copied: true });
    };

    shareViaEmail = async () => {
        const { email } = this.state;
        if (!validator.isEmail(email)) {
            return;
        }

        try {
            const result = await postService.sharePostLinkViaEmail({ link: this.postLink, email });
            if (!result.error) {
                NotificationManager.info('link was sent');
            }
        } finally {
            this.setState({ isEmailModalClosed: true, email: '', isEmailValid: true });
        }
    }

    onEmailChange = e => this.setState({ email: e.target.value });

    onEmailBlur = () => this.setState(state => ({ isEmailValid: validator.isEmail(state.email) }));

    render() {
        const { close } = this.props;
        const { copied, email, isEmailValid, isEmailModalClosed } = this.state;
        return (
            <Modal open onClose={close}>
                <Modal.Header className={styles.header}>
                    <span>Share Post</span>
                    {copied && (
                        <span>
                            <Icon color="green" name="copy" />
                            Copied
                        </span>
                    )}
                </Modal.Header>
                <Modal.Content>
                    <Input
                        fluid
                        action={{ color: 'teal', labelPosition: 'right', icon: 'copy', content: 'Copy', onClick: this.copyToClipboard }}
                        value={this.postLink}
                        ref={(input) => { this.input = input; }}
                    />
                    <Divider />

                    <Modal
                        size="mini"
                        open={!isEmailModalClosed}
                        onClose={() => this.setState({ isEmailModalClosed: true, email: '', isEmailValid: true })}
                        trigger={<Button icon="mail" onClick={() => this.setState({ isEmailModalClosed: false })} />}
                    >
                        <Modal.Header>Share via email</Modal.Header>
                        <Modal.Content>
                            <Modal.Description>
                                <Input
                                    fluid
                                    icon="at"
                                    iconPosition="left"
                                    placeholder="email address"
                                    value={email}
                                    error={!isEmailValid}
                                    action={{ content: 'Send', onClick: this.shareViaEmail }}
                                    onChange={this.onEmailChange}
                                    onBlur={this.onEmailBlur}
                                />
                            </Modal.Description>
                        </Modal.Content>
                    </Modal>
                </Modal.Content>
            </Modal>
        );
    }
}

SharedPostLink.propTypes = {
    postId: PropTypes.string.isRequired,
    close: PropTypes.func.isRequired,
};

export default SharedPostLink;
