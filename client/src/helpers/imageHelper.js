export const getUserImgLink = image => (image
    ? image.link
    : 'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png');

export const getUserOrDefaultLink = link => link || 'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png';
