import postRepository from '../../data/repositories/post.repository';
import postReactionRepository from '../../data/repositories/post-reaction.repository';
import userRepository from '../../data/repositories/user.repository';
import * as imageService from './image.service';
import * as emailHelper from '../../helpers/email.helpers';

export const getPosts = (userId, filter) => postRepository.getPosts(userId, filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
    ...post,
    userId
});

export const setReaction = async (userId, { postId, isLike = true }) => {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
        ? postReactionRepository.deleteById(react.id)
        : postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await postReactionRepository.getPostReaction(userId, postId);

    // send email
    try {
        if (isLike && (!reaction || reaction.isLike === false)) {
            const postUser = await userRepository.getByPost(postId);
            if (userId !== postUser.id) {
                await emailHelper.sendLikePostEmail(postUser.email, postId);
            }
        }
    } catch (error) {
        console.error(error);
    }
    //

    const result = reaction
        ? await updateOrDelete(reaction)
        : await postReactionRepository.create({ userId, postId, isLike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const updatePost = async (userId, postId, post) => {
    console.log('post', post);
    if (userId === post.userId && postId === post.id) {
        const oldPost = await getPostById(postId);

        if (post.imageId !== undefined && oldPost.imageId !== null) {
            await imageService.deleteImage(oldPost.imageId);
        }

        return postRepository.updateById(postId, post);
    }

    return Promise.reject();
};

export const deletePost = async (userId, postId) => {
    const post = await postRepository.getPostById(postId);
    if (post && post.user.id === userId) {
        return postRepository.deleteById(postId);
    }

    return false;
};

export const getLikedUsersByPostId = async postId => postReactionRepository.getLikedUsers(postId);

export const shareLinkViaEmail = async ({ link = '', email = '' }) => {
    try {
        await emailHelper.shareLinkViaEmail(email, link);
        return { error: false };
    } catch (error) {
        console.error(error);
        return { error: true };
    }
};
