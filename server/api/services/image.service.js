import axios from 'axios';
import { imgurId } from '../../config/imgur.config';
import imageRepository from '../../data/repositories/image.repository';

const uploadToImgur = async (file) => {
    try {
        const { data: { data } } = await axios.post(
            'https://api.imgur.com/3/upload',
            {
                image: file.buffer.toString('base64')
            }, {
                headers: { Authorization: `Client-ID ${imgurId}` }
            }
        );
        return {
            link: data.link,
            deleteHash: data.deletehash
        };
    } catch ({ response: { data: { status, data } } }) { // parse Imgur error
        return Promise.reject({ status, message: data.error });
    }
};

const deleteFromImgur = async hash => axios.delete(
    `https://api.imgur.com/3/image/${hash}`,
    { headers: { Authorization: `Client-ID ${imgurId}` } }
);


export const upload = async (file) => {
    const image = await uploadToImgur(file);
    return imageRepository.create(image);
};

export const deleteImage = async (imageId) => {
    const { deleteHash } = await imageRepository.getById(imageId);
    const { data: { success: result } } = await deleteFromImgur(deleteHash);
    if (result) {
        imageRepository.deleteById(imageId);
    }
};
