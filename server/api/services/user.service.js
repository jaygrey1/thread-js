import userRepository from '../../data/repositories/user.repository';
import * as imageService from './image.service';

const validate = (string = '') => {
    const pattern = /^[a-zA-Z]\w{1,20}$/gm;
    return pattern.test((string).trim());
};

const validateUsername = async (username) => {
    if (!validate(username)) {
        return { error: true, message: 'invalid username' };
    }

    const user = await userRepository.getByUsername(username);
    if (user) {
        return { error: true, message: 'this username already taken, please choose another one' };
    }

    return { error: false };
};

const validateImage = async () => ({ error: false });


export const getUserById = async (userId) => {
    const { id, username, email, imageId, image, status } = await userRepository.getUserById(userId);
    return { id, username, email, imageId, image, status };
};

export const update = async (userId, request) => {
    try {
        if ('username' in request) {
            const result = validateUsername(request.username);
            if (result.error) {
                return result;
            }
        }

        if ('imageId' in request) {
            const result = validateImage(request.imageId);
            if (result.error) {
                return result;
            }
            // remove old image
            const { imageId: oldImageId } = await getUserById(userId);
            imageService.deleteImage(oldImageId);
        }

        const result = await userRepository.updateById(userId, request);
        return { error: false, result };
    } catch (error) {
        return { error: true, message: error };
    }
};
