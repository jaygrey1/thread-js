import tokenHelper from '../../helpers/token.helper';
import cryptoHelper from '../../helpers/crypto.helper';
import userRepository from '../../data/repositories/user.repository';
import * as resetTokenService from './resetToken.service';
import { sendResetPasswordEMail } from '../../helpers/email.helpers';

export const login = async ({ id }) => ({
    token: tokenHelper.createToken({ id }),
    user: await userRepository.getUserById(id)
});

export const register = async ({ password, ...userData }) => {
    const newUser = await userRepository.addUser({
        ...userData,
        password: await cryptoHelper.encrypt(password)
    });
    return login(newUser);
};

export const resetPassword = async ({ email }) => {
    try {
        const user = await userRepository.getByEmail(email);

        if (user) {
            const hash = await resetTokenService.generateHash(user);
            await sendResetPasswordEMail(email, hash);

            return { error: false };
        }
    } catch (error) {
        console.log(error);
        return { error: true, message: 'Can not send email with further instructions, try again later.' };
    }

    return { error: true, message: 'User with specified email not found.' };
};

const isPasswordValid = (password) => {
    return password.trim().length > 0;
};

export const changePassword = async ({ token = '', password = '' }) => {
    try {
        const resetToken = await resetTokenService.getByToken(token);
        const NOW = new Date().getTime();

        if (resetToken
            && (resetToken.expiredAt.getTime() > NOW)
            && isPasswordValid(password)
        ) {
            await userRepository.updateById(
                resetToken.user.id,
                { password: cryptoHelper.encryptSync(password) }
            );
            await resetTokenService.deleteToken(resetToken);
            return { error: false };
        }
        return { error: true };
    } catch (error) {
        console.error(error);
        return { error: true };
    }
};
