import resetTokenRepository from '../../data/repositories/reset-token.repository';

const MILISECONDS_IN_24H = 24 * 60 * 60 * 1000;
const AFTER_24_HOURS = (new Date()).getTime() + MILISECONDS_IN_24H;


export const generateHash = async (user) => {
    const hash = await resetTokenRepository.getByUserId(user.id);

    if (hash && hash.expiredAt.getTime() > AFTER_24_HOURS) {
        await resetTokenRepository.deleteById(hash.id);
    }

    if (!hash) {
        const { token } = await resetTokenRepository.create({
            userId: user.id,
            expiredAt: new Date(AFTER_24_HOURS)
        });
        return token;
    }

    return hash.token;
};

export const deleteToken = async ({ id }) => resetTokenRepository.deleteById(id);

export const getByToken = async token => resetTokenRepository.getByToken(token);
