import commentRepository from '../../data/repositories/comment.repository';
import commentReactionRepository from '../../data/repositories/comment-reaction.repository';

export const create = (userId, comment) => commentRepository.create({
    ...comment,
    userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const update = (userId, commentId, comment) => ((userId === comment.user.id)
    ? commentRepository.updateById(commentId, comment)
    : Promise.reject());

export const deleteComment = async (userId, commentId) => {
    const comment = await getCommentById(commentId);
    if (comment && comment.user.id === userId) {
        return commentRepository.deleteById(commentId);
    }

    return false;
};

export const setReaction = async (userId, { commentId, isLike }) => {
    const result = { commentId, like: 0, dislike: 0 };

    try {
        let reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

        if (reaction) {
            if (reaction.isLike === isLike) {
                await commentReactionRepository.deleteById(reaction.id);
                return isLike
                    ? { ...result, like: -1, dislike: 0 } : { ...result, like: 0, dislike: -1 };
            }
            await commentReactionRepository.updateById(reaction.id, { isLike });
            return isLike
                ? { ...result, like: 1, dislike: -1 } : { ...result, like: -1, dislike: 1 };
        }
        reaction = await commentReactionRepository.create({ userId, commentId, isLike });
        return isLike ? { ...result, like: 1, dislike: 0 } : { ...result, like: 0, dislike: 1 };
    } catch (error) {
        return result;
    }
};

export const getLikedUsers = async commentId => commentReactionRepository.getLikedUsers(commentId);
