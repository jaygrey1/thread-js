import { Router } from 'express';
import * as commentService from '../services/comment.service';

const router = Router();

router
    .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
        .then(comment => res.send(comment))
        .catch(next))
    .get('/who-likes/:id', (req, res, next) => commentService.getLikedUsers(req.params.id)
        .then(users => res.send(users))
        .catch(next))
    .post('/', (req, res, next) => commentService.create(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then(comment => res.send(comment))
        .catch(next))
    .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
        .then(result => res.send(result))
        .catch(next))
    .put('/:id', (req, res, next) => commentService.update(req.user.id, req.params.id, req.body)
        .then(comment => res.send(comment))
        .catch(next))
    .delete('/:id', (req, res, next) => commentService.deleteComment(req.user.id, req.params.id)
        .then(result => (result ? res.sendStatus(204) : res.sendStatus(400)))
        .catch(next));

export default router;
