import { Router } from 'express';
import * as userService from '../services/user.service';

const router = Router();

router
    .put('/', (req, res, next) => userService.update(req.user.id, req.body)
        .then(result => res.send(result))
        .catch(next));

export default router;
