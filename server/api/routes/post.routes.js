import { Router } from 'express';
import * as postService from '../services/post.service';

const router = Router();

router
    .get('/', (req, res, next) => postService.getPosts(req.user.id, req.query)
        .then(posts => res.send(posts))
        .catch(next))
    .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
        .then(post => res.send(post))
        .catch(next))
    .get('/who-likes/:id', (req, res, next) => postService.getLikedUsersByPostId(req.params.id)
        .then(users => res.send(users))
        .catch(next))
    .post('/share/email/', (req, res, next) => postService.shareLinkViaEmail(req.body)
        .then(result => res.send(result))
        .catch(next))
    .post('/', (req, res, next) => postService.create(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then((post) => {
            req.io.emit('new_post', post); // notify all users that a new post was created
            return res.send(post);
        })
        .catch(next))
    .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then((reaction) => {
            if (reaction.post && (reaction.post.userId !== req.user.id)) {
                // notify a user if someone (not himself) liked his post
                req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
            }
            return res.send(reaction);
        })
        .catch(next))
    .put('/:id', (req, res, next) => postService.updatePost(req.user.id, req.params.id, req.body)
        .then((post) => {
            req.io.emit('update_post', post);
            return res.send(post);
        })
        .catch(next))
    .delete('/:id', (req, res, next) => postService.deletePost(req.user.id, req.params.id)
        .then((result) => {
            if (result) {
                return res.sendStatus(204);
            }
            return res.sendStatus(400);
        })
        .catch(next));

export default router;
