import dotenv from 'dotenv';

dotenv.config();

export const username = process.env.MAILJET_PUBLIC;
export const password = process.env.MAILJET_SECRET;
export const fromEmail = process.env.MAILJET_SENDER_EMAIL;
export const hostnameLink = process.env.MAILJET_HOSTNAME_LINK;
