export default [
    '/auth/login',
    '/auth/register',
    '/auth/reset-password',
    '/auth/change-password'
];
