import axios from 'axios';
import * as mailjet from '../config/mailjet.config';


function buildMessage(from, to, subject, textBody, htmlBody = '') {
    return {
        Messages: [
            {
                From: {
                    Email: from
                },

                To: [{
                    Email: to
                }],

                Subject: subject,

                TextPart: textBody,

                HTMLPart: htmlBody
            }
        ]
    };
}

async function sendEMail(to, subject, textBody, htmlBody = '') {
    const message = buildMessage(mailjet.fromEmail, to, subject, textBody, htmlBody);

    const config = {
        auth: {
            username: mailjet.username,
            password: mailjet.password,
        },
    };
    try {
        const result = await axios.post('https://api.mailjet.com/v3.1/send', message, config);
        return { error: false, ...result };
    } catch (error) {
        console.log('error **sendEMail**', error);
        throw error;
    }
}

export async function sendResetPasswordEMail(to, hash) {
    const page = `${mailjet.hostnameLink}change-password/`;
    const link = `${page}?token=${hash}`;

    const textMessageBody = `Dear user!
    Someone want to reset password to your account. if it wasn't you
    just ignore this letter, otherwise follow ${link} to change your password,
    or open this ${page} in the browser and use ${hash} as secure token.`;

    const htmlMessageBody = `
    <h3>Dear user!</h3>
    <p>
    Someone want to reset password to your account. if it wasn't you
    just ignore this letter, otherwise follow this <a href="${link}">link</a> to change your password,
    or open this url ${page} in the browser and use <b>${hash}</b> as secure token.
    </p>
    `;

    return sendEMail(to, 'Thread :: Reset password Instructions', textMessageBody, htmlMessageBody);
}

export async function sendLikePostEmail(to, postId) {
    const link = `${mailjet.hostnameLink}share/${postId}`;
    const textMessageBody = `Someone just liked your post, follow this link ${link} to find out who exactly.`;
    const htmlMessageBody = `
    <p>
    Someone just liked your post, follow this <a href="${link}">link</a> to find out who exactly.
    </p>
    `;
    return sendEMail(to, 'Thread :: Someone liked your post', textMessageBody, htmlMessageBody);
}


export async function sendSharePostEmail(to, link) {
    const textMessageBody = `You should defenetely see this post ${link} .`;
    const htmlMessageBody = `
    <p>
        You should defenetely see <a href="${link}">this</a> post.
    </p>
    `;

    return sendEMail(to, 'Thread :: shared post', textMessageBody, htmlMessageBody);
}
