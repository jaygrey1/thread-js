import sequelize from '../db/connection';
import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './base.repository';

const likeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class CommentRepository extends BaseRepository {
    getCommentById(id) {
        return this.model.findOne({
            attributes: {
                include: [
                    [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
                ],
                exclude: ['deletedAt']
            },
            group: [
                'comment.id',
                'user.id',
                'user->image.id'
            ],
            where: { id },
            include: [{
                model: UserModel,
                attributes: ['id', 'username', 'status'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: CommentReactionModel,
                attributes: [],
                duplicating: false
            }]
        });
    }
}

export default new CommentRepository(CommentModel);
