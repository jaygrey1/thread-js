import { UserModel, ImageModel, PostModel } from '../models/index';
import BaseRepository from './base.repository';

class UserRepository extends BaseRepository {
    addUser(user) {
        return this.create(user);
    }

    getByEmail(email) {
        return this.model.findOne({ where: { email } });
    }

    getByUsername(username) {
        return this.model.findOne({ where: { username } });
    }

    getByPost(postId) {
        return this.model.findOne({
            include: {
                model: PostModel,
                attributes: [],
                where: { id: postId }
            }
        });
    }

    getUserById(id) {
        return this.model.findOne({
            group: [
                'user.id',
                'image.id'
            ],
            where: { id },
            include: {
                model: ImageModel,
                attributes: ['id', 'link']
            }
        });
    }
}

export default new UserRepository(UserModel);
