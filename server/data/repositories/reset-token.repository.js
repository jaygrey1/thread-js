import { ResetTokenModel, UserModel } from '../models/index';
import BaseRepository from './base.repository';

class ResetPasswordRepository extends BaseRepository {
    async getByUserId(userId) {
        return this.model.findOne(
            {
                where:
                    { userId }
            }
        );
    }

    async getByToken(token) {
        return this.model.findOne({
            where: { token },
            include: {
                model: UserModel
            }
        });
    }
}

export default new ResetPasswordRepository(ResetTokenModel);
