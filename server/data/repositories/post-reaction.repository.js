import { PostReactionModel, PostModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './base.repository';
import sequelize from '../db/connection';

class PostReactionRepository extends BaseRepository {
    getPostReaction(userId, postId) {
        return this.model.findOne({
            group: [
                'postReaction.id',
                'post.id'
            ],
            where: { userId, postId },
            include: [{
                model: PostModel,
                attributes: ['id', 'userId']
            }]
        });
    }

    getLikedUsers(postId) {
        return this.model.findAll({
            attributes: [
                [sequelize.literal('"user"."username"'), 'username'],
                [sequelize.literal('"user->image"."link"'), 'link'],
            ],
            where: { postId, isLike: true },
            include: {
                model: UserModel,
                attributes: [],
                include: {
                    model: ImageModel,
                    attributes: [],
                },
            },
            order: sequelize.col('username')
        });
    }
}

export default new PostReactionRepository(PostReactionModel);
