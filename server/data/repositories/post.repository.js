import { Op } from 'sequelize';
import sequelize from '../db/connection';
import { PostModel, CommentModel, UserModel, ImageModel, PostReactionModel } from '../models/index';
import BaseRepository from './base.repository';

class PostRepository extends BaseRepository {
    async getPosts(user, filter) {
        let likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

        const {
            from: offset,
            count: limit,
        } = filter;

        const userId = filter.userId ? user : undefined;
        const excludeUserId = filter.excludeUserId ? user : undefined;
        const likedByUserId = filter.likedByUserId ? user : undefined;

        let where;
        const includedModels = [];

        if (userId) {
            where = { userId };
        }
        if (excludeUserId) {
            where = where
                ? { [Op.and]: [where, { userId: { [Op.ne]: excludeUserId } }] }
                : { userId: { [Op.ne]: excludeUserId } };
        }


        const postReactionModelInclude = {
            model: PostReactionModel,
            attributes: [],
            duplicating: false,
        };

        if (likedByUserId) {
            postReactionModelInclude.where = {
                [Op.and]: [
                    { isLike: { [Op.eq]: true } },
                    { userId: { [Op.eq]: likedByUserId } }
                ]
            };

            includedModels.push({
                model: PostReactionModel,
                as: 'postReactionForCount',
                attributes: [],
                duplicating: false,
            });

            likeCase = bool => `CASE WHEN "postReactionForCount"."isLike" = ${bool} THEN 1 ELSE 0 END`;
        }

        includedModels.push(...[
            {
                model: ImageModel,
                attributes: ['id', 'link']
            },
            {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }
        ]);

        includedModels.push(postReactionModelInclude);

        return this.model.findAll({
            where,
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" and "comment"."deletedAt" is null)`), 'commentCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
                ],
                exclude: ['deletedAt']
            },
            include: includedModels,
            group: [
                'post.id',
                'image.id',
                'user.id',
                'user->image.id'
            ],
            order: [['createdAt', 'DESC']],
            offset,
            limit
        });
    }

    getPostById(id) {
        const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
        return this.model.findOne({
            group: [
                'post.id',
                'comments.id',
                'comments->user.id',
                'comments->user->image.id',
                'user.id',
                'user->image.id',
                'image.id'
            ],
            where: { id },
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" and "comment"."deletedAt" is null)`), 'commentCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
                ],
                exclude: ['deletedAt']
            },
            include: [{
                model: CommentModel,
                attributes: {
                    include: [
                        [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "commentReactions"
                        WHERE "comments"."id" = "commentReactions"."commentId" and "commentReactions"."isLike" = True and "comments"."deletedAt" is null)`), 'likeCount'],
                        [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "commentReactions"
                        WHERE "comments"."id" = "commentReactions"."commentId" and "commentReactions"."isLike" = False and "comments"."deletedAt" is null)`), 'dislikeCount'],
                    ],
                    exclude: ['deletedAt']
                },
                include: {
                    model: UserModel,
                    attributes: ['id', 'username', 'status'],
                    include: {
                        model: ImageModel,
                        attributes: ['id', 'link']
                    }
                }
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: PostReactionModel,
                attributes: []
            }]
        });
    }
}

export default new PostRepository(PostModel);
