import { CommentReactionModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './base.repository';
import sequelize from '../db/connection';

class CommentReactionRepository extends BaseRepository {
    getCommentReaction(userId, commentId) {
        return this.model.findOne({
            where: { userId, commentId }
        });
    }

    getLikedUsers(commentId) {
        return this.model.findAll({
            attributes: [
                [sequelize.literal('"user"."username"'), 'username'],
                [sequelize.literal('"user->image"."link"'), 'link'],
            ],
            where: { commentId, isLike: true },
            include: {
                model: UserModel,
                attributes: [],
                include: {
                    model: ImageModel,
                    attributes: []
                }
            },
            order: sequelize.col('username')
        });
    }
}

export default new CommentReactionRepository(CommentReactionModel);
