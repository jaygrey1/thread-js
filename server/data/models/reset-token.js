import Sequelize from 'sequelize';

export default (orm, DataTypes) => {
    const ResetToken = orm.define('resetToken', {
        token: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            defaultValue: Sequelize.UUIDV1
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
        expiredAt: DataTypes.DATE
    }, {});

    return ResetToken;
};
