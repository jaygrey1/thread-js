import { statuses } from '../seed-data/users.seed';

const randomIndex = length => Math.floor(Math.random() * length);

export default {
    up: async (queryInterface, Sequelize) => {
        try {
            const options = {
                type: Sequelize.QueryTypes.SELECT
            };

            const users = await queryInterface.sequelize.query('SELECT id FROM "users";', options);

            const promises = [];

            users.forEach(({ id }) => {
                const status = statuses[randomIndex(statuses.length)];
                const promise = queryInterface.sequelize.query(
                    'update "users" set status=:status where id = :id;',
                    {
                        type: Sequelize.QueryTypes.UPDATE,
                        replacements: { status, id }
                    },
                );
                promises.push(promise);
            });
            return Promise.all(promises);
        } catch (error) {
            console.log(error);
            return Promise.reject(error);
        }
    },

    down: async (queryInterface) => {
        try {
            await queryInterface.bulkUpdate('users', { status: null }, {});
        } catch (error) {
            console.log(error);
        }
    },
};
