
const randomRange = (min, max) => Math.random() * (max - min) + min;

export default {
    up: async (queryInterface, Sequelize) => {
        try {
            const options = {
                type: Sequelize.QueryTypes.SELECT
            };

            const users = await queryInterface.sequelize.query('SELECT id FROM "users";', options);
            const comments = await queryInterface.sequelize.query('SELECT id FROM "comments";', options);

            const now = new Date();
            const seedData = [];

            users.forEach((user) => {
                comments.forEach((comment) => {
                    const number = randomRange(-1, 1);
                    const reaction = {
                        userId: user.id,
                        commentId: comment.id,
                        createdAt: now,
                        updatedAt: now
                    };
                    if (number > 0.5) {
                        seedData.push({
                            ...reaction,
                            isLike: true,
                        });
                    } else if (number < -0.5) {
                        seedData.push({
                            ...reaction,
                            isLike: false,
                        });
                    }
                });
            });

            await queryInterface.bulkInsert('commentReactions', seedData, {});
        } catch (error) {
            console.log(error);
        }
    },

    down: async (queryInterface) => {
        try {
            await queryInterface.bulkDelete('commentReactions', null, {});
        } catch (error) {
            console.log(error);
        }
    },
};
