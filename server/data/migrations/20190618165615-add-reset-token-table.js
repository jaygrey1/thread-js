export default {
    up: (queryInterface, Sequelize) => queryInterface.sequelize
        .transaction(transaction => queryInterface.createTable('resetTokens', {
            id: {
                allowNull: false,
                autoIncrement: false,
                primaryKey: true,
                type: Sequelize.UUID,
                defaultValue: Sequelize.literal('gen_random_uuid()')
            },
            userId: {
                type: Sequelize.UUID,
                references: {
                    model: 'users',
                    key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            },
            token: {
                allowNull: false,
                unique: true,
                type: Sequelize.UUID,
                defaultValue: Sequelize.literal('gen_random_uuid()')
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE,
            expiredAt: Sequelize.DATE
        }, { transaction })),

    down: queryInterface => queryInterface.sequelize
        .transaction(transaction => queryInterface.dropTable('resetTokens', { transaction }))
};
